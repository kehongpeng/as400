'use strict';

var request        =  require('request'),
	async          =  require('async');
var config         =  require('../config/settings.server.config.js').configs;
var logger         =  require('../utils/logger.server.util.js'),
    cacheService   = require('../services/cache.server.service.js');


function getCachedUser(tempStore, callBack) {
	cacheService.get(tempStore.ticket, function(err, userprincipal) {
		if (err) {
			return callBack(err);			
		}
		tempStore.userprincipal = userprincipal;	
		return callBack(null, tempStore);		
	});	
}

function executeService(tempStore, callBack) {
	var username        = tempStore.userprincipal.username,
	    service         = tempStore.module,
	    requestBody     = "user=" + tempStore.config.serviceUser +  "&password=" + tempStore.config.servicePassword + "&request="  + tempStore.service;	
	logger.info(__filename, "In service request processing...");	    
	
	var endpoint = tempStore.config.baseUrl + "?" + 
	               "user=" + tempStore.config.serviceUser +  "&password=" + tempStore.config.servicePassword + 
	               "&request="  + tempStore.service + 
	               "&student="  + username;
    logger.info(__filename, " Url endpoint: ", endpoint);	               
	request({
		url: endpoint,
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: '',
		timeout: tempStore.config.serviceTimeout
	}, function(err, response, body) {
		if (err) {
			return callBack(err);
		}
		tempStore.body = body;
		return callBack(null, tempStore);
	});	
}

exports.processGetRequest = function(req, res) {
	var tempStore = {
		"ticket": req.query.ticket,
		"service": req.params.module,
		"config": config
	}
	logger.info(__filename, "- ticket=", req.query.ticket, " : service=", req.params.module);
	async.waterfall([
			async.apply(getCachedUser, tempStore),
			executeService
	], function(err, tempStore) {
		if (err) {
			res.writeHead(500, {
				'Content-Type': 'application/text'
			});					
			res.end("Error occurred while fetching the data from server");
			return;			
		} else {
			res.writeHead(200, {
				'Content-Type': 'application/text'
			});			
			res.end(tempStore.body);	
			return;				
		}
	});
	
}

exports.processPublicGetRequest = function(req, res) {
	var tempStore = {
		"service" : req.params.module,
		"config"  : config
	}
	logger.info(__filename, "- Executing Public service=", req.params.module);
	var endpoint = tempStore.config.baseUrl + "?" + 
	               "user=" + tempStore.config.serviceUser +  "&password=" + tempStore.config.servicePassword + 
	               "&request="  + tempStore.service;
    logger.info(__filename, " Url endpoint: ", endpoint);	               
	request({
		url: endpoint,
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: '',
		timeout: tempStore.config.serviceTimeout
	}, function(err, response, body) {
		if (err) {
			res.writeHead(500, {
				'Content-Type': 'application/text'
			});					
			res.end("Error occurred while fetching the data from server");
			return;	
		}
			res.writeHead(200, {
				'Content-Type': 'application/text'
			});			
			res.end(body);	
			return;			

	});		
}