'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Authentication Controller
//	authenticate : function is called from auth.server.routes for all
//  auth requests from  users. 
////////////////////////////////////////////////////////////////////////////////
var request     = require('request'),
    async = require('async'),
	logger      = require('../utils/logger.server.util.js'),
	config      = require('../config/settings.server.config.js'),
	principal   = require('../models/UserPrincipal'),
	ldapAuthService = require('../services/ldapauth.server.service.js'),
	ticketGen   = require('../utils/ticketgenerator.server.util'),
	cacheService = require('../services/cache.server.service.js');

function callAuth(tempStore, callBack) {
	ldapAuthService.authenticate(tempStore, function(err, resTempStore) {
		if (err) {
			return callBack(err);			
		} 
		return callBack(null, resTempStore);
	});	
}

function generateTicket(tempStore, callBack) {
	var ticket = ticketGen();
	tempStore.ticket = ticket;
	return callBack(null, tempStore);
}

function addToCache(tempStore, callBack) {
	cacheService.add(tempStore.ticket, tempStore.userPrincipal, function(err, success) {
		if (err) {
			return callBack(err);
		}
		return callBack(null, tempStore);
	});
}

exports.authenticate = function(req,res) {
	logger.info(__filename, "- Authenticating user ", req.body.username, "using URL", req.url);
	// Get username & password from the request body
	var	username     = req.body.username,
		password     = decodeURIComponent(req.body.password),
		outResponse;
	// Return error if username is blank
	if (!username) {
		res.writeHead(500, {
			'Content-Type': 'application/json'
		});
		outResponse = {
			"error": "Username cannot be blank !"
		}
		res.end(JSON.stringify(outResponse));
		return;
	}
	// Authenticate user
	var tempStore = {
		"username" : username,
		"password" : password
	};
	async.waterfall([
			async.apply(callAuth, tempStore),
			generateTicket,
			addToCache
		], function(err, tempStore) {
			if (err) {
				// Error authenticating
				logger.error(__filename, "- Authenticating failed for user ", req.body.username + "Error is: ");
				logger.error(__filename, err.error);
				res.writeHead(500, {
					'Content-Type': 'application/json'
				});
				res.end("Internal server error");				
			} else {
				// Send ticket as response
				outResponse = {
					"ticket": tempStore.ticket,
					"roles": ""
				}			
				res.writeHead(200, {
					'Content-Type': 'application/json'
				});			
				res.end(JSON.stringify(outResponse));				
			}
		});

}