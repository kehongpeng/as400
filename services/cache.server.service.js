const NodeCache = require('node-cache');
const cache = new NodeCache({ stdTTL: 3600, checkperiod: 3620 });


exports.add = function(key,value, callBack) {
	cache.set(key, value, function(err, success) {
		return callBack(err, success);
	});
	
}

exports.get = function(key, callBack) {
	cache.get(key, function(err, value) {
		return callBack(err, value);
	});	
}

exports.del = function(key, callBack) {
	cache.del(key, function(err, count) {
		return callBack(err, count);
	});
}